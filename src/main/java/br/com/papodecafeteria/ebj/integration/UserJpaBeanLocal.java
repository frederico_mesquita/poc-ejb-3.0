package br.com.papodecafeteria.ebj.integration;

import java.util.List;

import javax.ejb.Local;

import br.com.papodecafeteria.dao.model.UserJPA;

@Local
public interface UserJpaBeanLocal {
	public int save(UserJPA pUser);
	public int update(UserJPA pUser);
	public int delete(UserJPA pUser);
	public List<UserJPA> getAllRecords();
	public UserJPA getRecordById(int pId);
}
