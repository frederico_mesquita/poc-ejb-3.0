package br.com.papodecafeteria.ebj;

import br.com.papodecafeteria.dao.jpa.UserDaoJpa;
import br.com.papodecafeteria.dao.model.UserJPA;
import br.com.papodecafeteria.ebj.integration.UserJpaBeanLocal;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;

@Stateless
public class UserJpaBean implements UserJpaBeanLocal {
	private static Logger l = Logger.getLogger(UserJpaBean.class.getName());

	public static Logger getL() {
		return l;
	}

	public int save(UserJPA pUser){
		int status = 0;
		try{
			status = UserDaoJpa.save(pUser);
		} catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    } 
		return status;
	}
	
	public int update(UserJPA pUser){
		int status = 0;
		try{
			status = UserDaoJpa.update(pUser);
		} catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    } 
		return status;
	}
	
	public int delete(UserJPA pUser){
		int status = 0;
		try{
			status = UserDaoJpa.delete(pUser);
		} catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    } 
		return status;
	}
	
	public List<UserJPA> getAllRecords(){
		List<UserJPA> lstUser = null;
		try{
			lstUser = UserDaoJpa.getAllRecords();
		} catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    } 
		return lstUser;
	}
	
	public UserJPA getRecordById(int pId){
		UserJPA user = null;
		try{
			user = UserDaoJpa.getRecordById(pId);
		} catch(Exception exc){
	    	getL().log(Level.SEVERE, exc.getMessage(), exc);
	    } 
		return user;
	}
}
